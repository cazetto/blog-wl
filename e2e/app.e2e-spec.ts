import { BlogWlPage } from './app.po';

describe('blog-wl App', () => {
  let page: BlogWlPage;

  beforeEach(() => {
    page = new BlogWlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
