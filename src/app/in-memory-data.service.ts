import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {

  createDb() {

    let posts = [
      {id: 1, title: 'Lorem ipsum', message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'},
      {id: 2, title: 'Ut enim', message: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'},
      {id: 3, title: 'Duis aute', message: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'},
      {id: 4, title: 'Excepteur sint', message: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},
      {id: 5, title: 'Reprehenderit in', message: 'Reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'},
      {id: 6, title: 'Consectetur adipisicing', message: 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'},
      {id: 7, title: 'Enim ad minim', message: 'Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'},
    ];

    console.table(posts);

    return { posts };
  }
}
