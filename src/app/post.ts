export default class Post {
  id: number;
  title: string;
  message: string;
}
