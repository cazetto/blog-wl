import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import Post from '../post';
import { PostsService } from '../posts.service';

import 'rxjs/add/operator/switchMap';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  providers: [ PostsService ]
})
export class PostComponent implements OnInit {

  constructor(
    private postsService:PostsService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  post: Post;
  isEditing: boolean;
  isCreating: boolean;
  saveMethod: any;

  ngOnInit() {
    const { path:lastRouteFragment } = this.route.snapshot.url[this.route.snapshot.url.length-1];

    // dangerous!?
    this.isEditing = lastRouteFragment === 'edit';
    this.isCreating = lastRouteFragment === 'post';
    this.saveMethod = this.isCreating ? 'createPost' : 'updatePost';

    if(this.isCreating) this.post = new Post();

    this.route.params
      .switchMap((params: Params) => this.postsService.getPost(+params['id']))
      .subscribe(post => this.post = post);
  }

  save(post: Post): void {
    this.postsService[this.saveMethod](post)
    .then(post => {
    })
  }

  cancel(): void {
    this.location.back();
  }

}
