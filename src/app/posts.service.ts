import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Location } from '@angular/common';

import 'rxjs/add/operator/toPromise';

import Post from './post';

@Injectable()
export class PostsService {

  // private apiUrl = 'http://138.197.105.195/api/v1/posts/post';
  private apiUrl = 'api/posts';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(
    private http: Http,
    private location: Location
  ) { }

  // Get
  getPosts(): Promise<Post[]> {
    return this.http
      .get(this.apiUrl)
      .toPromise()
      .then(response => response.json().data as Post[])
      .catch(this.handleError);
  }

  // Get especific Post by id
  getPost(id: number) {
    const url = `${this.apiUrl}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => response.json().data as Post)
      .catch(this.handleError);
  }
  // Create new
  createPost(post: Post) {
    return this.http
      .post(this.apiUrl, JSON.stringify({title: post.title, message: post.message}), {headers: this.headers})
      .toPromise()
      .then(res => {
        this.location.back();
      })
      .catch(this.handleError);
  }
  // Update especific post
  updatePost(post: Post) {
    const url = `${this.apiUrl}/${post.id}`;
    return this.http
      .put(url, JSON.stringify(post), {headers: this.headers})
      .toPromise()
      .then(res => {
        this.location.back();
      })
      .catch(this.handleError);
  }
  // Delete especific post by id
  deletePost(id: number) {
    const url = `${this.apiUrl}/${id}`;
    return this.http
    .delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  // HTTP Errors
  private handleError(error: any): Promise<any> {
    console.error(`Oops, we have a HTTP error: ${error}`);
    return Promise.reject(error.message || error);
  }
}
