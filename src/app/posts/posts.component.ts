import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import Post from '../post';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [ PostsService ]
})
export class PostsComponent implements OnInit {

  constructor(
    private postsService:PostsService,
    private router: Router
  ) { }

  posts: Post[] = [];

  getPosts(): void {
    this.postsService
    .getPosts()
    .then(posts => this.posts = posts);
  }

  deletePost(post: Post): void {
    this.postsService
    .deletePost(post.id)
    .then(() => {
      this.posts = this.posts.filter(h => h !== post);
    })
  }

  edit(post: Post): void {
    this.router.navigate(['/post', post.id, 'edit']);
  }

  ngOnInit(): void {
    this.getPosts();
  }
}
